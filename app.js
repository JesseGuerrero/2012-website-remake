let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let exphbs  = require('express-handlebars');
let methodOverride = require('method-override');
let indexRouter = require('./routes/index');
let articlesRouter = require('./routes/articles')
let usersRouter = require('./routes/users');
const http = require('http')
const axios = require('axios')
const compression = require('compression');
require('dotenv').config()
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
process.on('uncaughtException', function (error) {
  console.log(error.stack);
});

const {authenticate, isModerator, formatRSNickName} = require("./utils/utils");
let app = express();

//compress
app.use(compression());

// view engine setup
const hbs = require('./utils/helpers')(exphbs);

app.engine('hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(methodOverride('_method'))
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'), {maxAge: '1d'}));

const playerCount = (req, res, next) => {
  axios.get(process.env.WEB_API_URL+'world/online')
      .then(response => {
        // Store the API response in a variable or pass it to the 'req' object for later use
        req.playerCount = response.data.count;
        next(); // Call next() to proceed to the next middleware or the route handler
      })
      .catch(error => {
        // Handle any errors that occurred during the API call
        console.error('API call error:', error);
        next(error); // Pass the error to the error handler middleware
      });
};
const worldsList = (req, res, next) => {
  axios.get(process.env.WEB_API_URL + "world")
      .then(response => {
        // Store the API response in a variable or pass it to the 'req' object for later use

        let worldList = response.data;
        for (let i = 0; i < worldList.length; i++) {
            // Capitalize the first letter of each word
            worldList[i] = worldList[i].replace(/\b\w/g, match => match.toUpperCase());
        }
        req.worldList = worldList;
        next(); // Call next() to proceed to the next middleware or the route handler
      })
      .catch(error => {
        // Handle any errors that occurred during the API call
        console.error('API call error:', error);
        next(error); // Pass the error to the error handler middleware
      })
};
const usernameOnAuth = (req, res, next) => {
    if (req.cookies && req.cookies.jwt && req.cookies.username)
        res.locals.usernameOnAuth = formatRSNickName(req.cookies.username);
    next();
};
app.use(usernameOnAuth);
app.use(playerCount);
app.use(worldsList)
app.use('/', indexRouter);
app.get('/edit-news', async (req, res) => {
  if(isModerator(req))
    axios.get(process.env.WEB_API_URL + "web?page=1&limit=99999&type=0")
        .then((response) => {
          res.render('articles/index', { layout: "layout-writenews", webAPI: process.env.WEB_API_URL, articles: response["data"],
            isModerator: isModerator(req) });
        });
  else
    res.redirect('/');
})

app.get("/admin", (req, res) => {
  if(isModerator(req))
    res.redirect('/edit-news');
  else
    res.redirect('/')
});

app.use('/news', articlesRouter);
app.use('/users', usersRouter);

app.use((req, res, next) => {
    res.status(404).render("404");
});

var port = normalizePort(process.env.PORT || '40004');
app.set('port', port);

/**
 * Create HTTP server.
 */
const httpsServer = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

httpsServer.listen(port);
httpsServer.on('error', onError);
httpsServer.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  var addr = httpsServer.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  console.debug('Listening on ' + bind);
}
