FROM node:20-alpine

ENV PORT=8080
ENV NEWS_API_USERNAME=admin
ENV NEWS_API_PASSWORD=test
ENV NEWS_API_UPLOAD_TOKEN=meme
ENV WEB_API_URL=https://api.darkan.org/v1/

COPY --chown=node:node . .

USER node

CMD [ "node", "app.js"]