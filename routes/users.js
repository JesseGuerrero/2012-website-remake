var express = require('express');
var router = express.Router();
const axios = require('axios');
const { setCookieRights } = require("../utils/utils");

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/login', async (req, res) => {
    try {
        const apiUrl = process.env.WEB_API_URL + 'accounts/login';
        const username = req.body.username.toLowerCase().replace(" ", "_");
        const loginData = { username: username, password: req.body.password };

        const auth = await axios.post(apiUrl, loginData, {
            headers: {'Content-Type': 'application/json'}
        });

        const playerRights = await setCookieRights(username, auth.data["access_token"]);

        // Set cookies
        const cookieOptions = { maxAge: 48 * 60 * 60 * 1000 }; // 48 hours
        res.cookie('jwt', auth.data["access_token"], cookieOptions);
        res.cookie('username', username, cookieOptions);
        res.cookie('playerRights', playerRights, cookieOptions);

        res.redirect('/');
    } catch (error) {
        if (error.response) {
          if(error.response.status == 401)
            res.status(401).render("401")
          else
            res.status(error.response.status).send(error.response.data);
        } else if (error.request) {
            res.status(500).send('No response received');
        } else {
            res.status(500).send('Request setup error');
        }
    }
});








module.exports = router;
