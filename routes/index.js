let express = require('express');
const { getSkillIDByName, formatRSNickName, fetchSkills } = require("../utils/utils");
const axios = require('axios')
let router = express.Router();

/* GET home page. */
router.get('/', async (req, res, next) => {//https://web.archive.org/web/20120709052134/http://www.runescape.com:80/title.ws
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + "web?page=1&limit=6&type=0");
        const data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        res.render('index', { layouts: 'layout.hbs', articles: data['data'], playerCount: req.playerCount });
    } catch (err) {
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});

router.get('/world', function(req, res, next) {//https://web.archive.org/web/20120731001805/http://www.runescape.com:80/world_locations.ws
  res.render('pages/world/world', { layout: 'layout.hbs', world: true, playerCount: req.playerCount  });
});

router.get('/rs-wiki', function(req, res, next) {
  res.redirect('https://wiki.darkan.org/', {});
});


router.get('/downloads', function(req, res, next) { //https://web.archive.org/web/20120718053538/http://www.runescape.com/downloads.ws
  res.render('pages/downloads', { layout: 'layout.hbs', downloads: true, playerCount: req.playerCount  });
});

router.get('/hall-of-heroes', async function(req, res, next) { //https://web.archive.org/web/20120620024338/http://services.runescape.com/m=hiscore/heroes.ws
    res.redirect('hall-of-heroes/' + req.worldList[0]);
});

router.get('/hall-of-heroes/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120620024338/http://services.runescape.com/m=hiscore/heroes.ws
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + "highscores/" + req.params.worldName + "?page=1&limit=10");
        const data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        console.log(req.params.worldName)
        res.render('pages/hall-of-heroes', { layout: 'layout.hbs', heroes: true, highscores: data, playerCount: req.playerCount, worldName: req.params.worldName, worldList: req.worldList  });
    } catch (err) {
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});


// router.get('/temporal-hall-of-heroes', function(req, res, next) { //https://web.archive.org/web/20120620024338/http://services.runescape.com/m=hiscore/heroes.ws
//     res.redirect("/temporal-hall-of-heroes/0/1");
// });
//
// router.get('/temporal-hall-of-heroes/:pastday/:secondpastday', async function(req, res, next) { //https://web.archive.org/web/20120620024338/http://services.runescape.com/m=hiscore/heroes.ws
//     try {
//         const dataPromise = axios.get(process.env.WEB_API_URL + `/temporal?page=1&limit=10&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`);
//         const data = await Promise.race([
//             dataPromise,
//             new Promise((_, reject) =>
//                 setTimeout(() => reject('Request Timed Out'), 5000)
//             ),
//         ]);
//         res.render('pages/temporal-hall-of-heroes', { layout: 'layout.hbs', heroes: true, highscores: data,
//             firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
//     } catch (err) {
//         // Handle errors, including timeouts
//         res.status(500).render('error', { message: 'Something went wrong!' });
//     }
// });

router.get('/highscores', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    res.redirect('/highscores/' + req.worldList[0]);
});
router.get('/highscores/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + "highscores/" + req.params.worldName + "?page=" + 1 +"&limit=" + 22);
        const data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        res.render('pages/highscores', { layout: 'layout.hbs', highscore: true, highscores: data, skill: "Overall", page: 1,
            limit: 22, playerCount: req.playerCount, worldName: req.params.worldName, worldList: req.worldList });
    } catch (err) {
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});

// router.get('/temporal-highscores', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
//     try {
//         const dataPromise = axios.get(process.env.WEB_API_URL + "/temporal?page=" + 1 +"&limit=" + 10)
//         const data = await Promise.race([
//             dataPromise,
//             new Promise((_, reject) =>
//                 setTimeout(() => reject('Request Timed Out'), 5000)
//             ),
//         ]);
//         res.render('pages/temporal-highscores', { layout: 'layout.hbs', highscore: true, highscores: data, skill: "Overall", page: 1,
//             limit: 10, firstDayPast: 0, secondDayPast: 1, playerCount: req.playerCount });
//     } catch (err) {
//         // Handle errors, including timeouts
//         res.status(500).render('error', { message: 'Something went wrong!' });
//     }
// });

// router.get('/temporal-highscores/overall/:pastday/:secondpastday/:page', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
//     try {
//         const dataPromise = axios.get(process.env.WEB_API_URL + `/temporal?page=${req.params.page}&limit=10&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
//         const data = await Promise.race([
//             dataPromise,
//             new Promise((_, reject) =>
//                 setTimeout(() => reject('Request Timed Out'), 5000)
//             ),
//         ]);
//         res.render('pages/temporal-highscores', { layout: 'layout.hbs', highscore: true, highscores: data, skill: "Overall", page: req.params.page,
//             limit: 10, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
//     } catch (err) {
//         // Handle errors, including timeouts
//         res.status(500).render('error', { message: 'Something went wrong!' });
//     }
//
// });

// router.get('/temporal-highscores/:skill/:pastday/:secondpastday/:page/', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
//     try {
//         const dataPromise = axios.get(process.env.WEB_API_URL + `/temporal?page=${req.params.page}&skill=${getSkillIDByName(req.params.skill)}&limit=10&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
//         const data = await Promise.race([
//             dataPromise,
//             new Promise((_, reject) =>
//                 setTimeout(() => reject('Request Timed Out'), 5000)
//             ),
//         ]);
//         res.render('pages/temporal-highscores', { layout: 'layout.hbs', highscore: true, highscores: data, skill: req.params.skill,
//             page: req.params.page, limit: 10, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
//     } catch (err) {
//         // Handle errors, including timeouts
//         res.status(500).render('error', { message: 'Something went wrong!' });
//     }
// });

// router.get('/temporal-highscores-player/:user', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
//     try {
//         const dataPromise = axios.get(process.env.WEB_API_URL + `/temporal/player?username=${req.params.user}`)
//         let data = await Promise.race([
//             dataPromise,
//             new Promise((_, reject) =>
//                 setTimeout(() => reject('Request Timed Out'), 5000)
//             ),
//         ]);
//         let username = formatRSNickName(req.params.user)
//         data = data['data']
//         res.render('pages/temporal-player-highscores', { layout: 'layout.hbs', displayName: username, overallLevelsUp: data.overallLevelsUp,
//             totalXP: data.totalXp, skillXPs: data.xpDifferential, overallRank: data.overallRank, levelsUp: data.levelsUp, xpRanks: data.xpRanks,
//             playerHighscore: true, firstDayPast: 0, secondDayPast: 1, playerCount: req.playerCount });
//     } catch (err) {
//         // Handle errors, including timeouts
//         res.status(500).render('error', { message: 'Something went wrong!' });
//     }
// });


// router.get('/temporal-highscores-player/:user/:pastday/:secondpastday', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
//     try {
//         const dataPromise = axios.get(process.env.WEB_API_URL + `/temporal/player?username=${req.params.user}&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
//         let data = await Promise.race([
//             dataPromise,
//             new Promise((_, reject) =>
//                 setTimeout(() => reject('Request Timed Out'), 5000)
//             ),
//         ]);
//         let username = formatRSNickName(req.params.user)
//         data = data['data']
//         res.render('pages/temporal-player-highscores', { layout: 'layout.hbs', displayName: username, overallLevelsUp: data.overallLevelsUp,
//             totalXP: data.totalXp, skillXPs: data.xpDifferential, overallRank: data.overallRank, levelsUp: data.levelsUp, xpRanks: data.xpRanks,
//             playerHighscore: true, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
//     } catch (err) {
//         // Handle errors, including timeouts
//         res.status(500).render('error', { message: 'Something went wrong!' });
//     }
// });
//
// router.get('/temporal-chart/:user/:pastday/:secondpastday', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
//     try {
//         const dataPromise = axios.get(process.env.WEB_API_URL + `/temporal/player?username=${req.params.user}&firstDayPast=${req.params.pastday}&secondDayPast=${req.params.secondpastday}`)
//         let data = await Promise.race([
//             dataPromise,
//             new Promise((_, reject) =>
//                 setTimeout(() => reject('Request Timed Out'), 5000)
//             ),
//         ]);
//         let username = formatRSNickName(req.params.user)
//         data = data['data']
//         res.render('pages/temporal-player-chart', { layout: 'layout.hbs', displayName: username, overallLevelsUp: data.overallLevelsUp,
//             totalXP: data.totalXp, skillXPs: data.xpDifferential, overallRank: data.overallRank, levelsUp: data.levelsUp, xpRanks: data.xpRanks,
//             playerHighscore: true, firstDayPast: req.params.pastday, secondDayPast: req.params.secondpastday, playerCount: req.playerCount  });
//     } catch (err) {
//         // Handle errors, including timeouts
//         res.status(500).render('error', { message: 'Something went wrong!' });
//     }
// });


router.get('/grandexchange', function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    res.redirect('grandexchange/buy/1/'+ req.worldList[0]);
});

router.get('/grandexchange/buy/:page/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + "ge/" + req.params.worldName + "/buy?page=" + req.params.page +"&limit=" + 22)
        let data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        filteredList = { "data": [] }
        for(let i = 0; i < data['data'].length; i++)
            if(data['data'][i].state != 'FINISHED')
                filteredList["data"].push(data['data'][i])
        res.render('pages/grandexchange', { layout: 'layout.hbs', highscore: true, itemListing: filteredList, type: "buy", page: req.params.page,
            limit: 22, playerCount: req.playerCount, worldName: req.params.worldName, worldList: req.worldList });
    } catch (err) {
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});

router.get('/grandexchange/sell/:page/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + "ge/" + req.params.worldName + "/sell?page=" + req.params.page +"&limit=" + 22)
        let data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        filteredList = { "data": [] }
        for(let i = 0; i < data['data'].length; i++)
            if(data['data'][i].state != 'FINISHED')
                filteredList["data"].push(data['data'][i])
        res.render('pages/grandexchange', { layout: 'layout.hbs', highscore: true, itemListing: filteredList, type: "sell", page: req.params.page,
            limit: 22, playerCount: req.playerCount, worldName: req.params.worldName, worldList: req.worldList });
    } catch (err) {
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});

router.get('/highscores/Overall/:page/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + "highscores/" + req.params.worldName + "?page=" + req.params.page +"&limit=" + 22)
        let data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        res.render('pages/highscores', { layout: 'layout.hbs', highscore: true, highscores: data, skill: "Overall", page: req.params.page,
            limit: 22, playerCount: req.playerCount, worldName: req.params.worldName, worldList: req.worldList });
    } catch (err) {
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});

router.get('/highscores/:skill/:page/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + "highscores/" + req.params.worldName + "?skill=" + getSkillIDByName(req.params.skill) + "&page=" + req.params.page +"&limit=" + 22)
        let data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        res.render('pages/highscores', { layout: 'layout.hbs', highscore: true, highscores: data, skill: req.params.skill, page: req.params.page,
            limit: 22, playerCount: req.playerCount, worldName: req.params.worldName, worldList: req.worldList });
    } catch (err) {
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});

router.get('/highscores/:skill/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    const skill = req.params.skill;
    const worldName = req.params.worldName;
    if (isNaN(worldName))
        res.redirect(`/highscores/${skill}/1/${worldName}`);
    else
        res.redirect(`/highscores/${skill}/${worldName}/${req.worldList[0]}`);

});

router.get('/highscores-player/:user/:worldName', async function(req, res, next) { //https://web.archive.org/web/20120608083454/http://services.runescape.com:80/m=hiscore/overall.ws?category_type=0&table=0
    try {
        const dataPromise = axios.get(process.env.WEB_API_URL + 'highscores/' + req.params.worldName + '?limit=9999999')
        let data = await Promise.race([
            dataPromise,
            new Promise((_, reject) =>
                setTimeout(() => reject('Request Timed Out'), 5000)
            ),
        ]);
        let username = formatRSNickName(req.params.user)
        let skillsData = fetchSkills(data, username);
        if (skillsData['totalRank'] == -1) {
            skillsData['totalLevel'] = 0;
            skillsData['totalXP'] = 0;
            skillsData['totalRank'] = 0;
            skillsData['skillXPs'] = Array(25).fill(0);
            skillsData['skillRanks'] = Array(25).fill(0);
        }
        res.render('pages/player-highscores', { layout: 'layout.hbs',
            displayName: username, totalLevel: skillsData['totalLevel'], totalXP: skillsData['totalXP'],
            totalRank: skillsData['totalRank'], skillXPs: skillsData['skillXPs'],
            skillRanks: skillsData['skillRanks'], playerHighscore: true, playerCount: req.playerCount, worldName: req.params.worldName, worldList: req.worldList  });
    } catch (err) {
        console.error(err)
        // Handle errors, including timeouts
        res.status(500).render('error', { message: 'Something went wrong!' });
    }
});

module.exports = router;
